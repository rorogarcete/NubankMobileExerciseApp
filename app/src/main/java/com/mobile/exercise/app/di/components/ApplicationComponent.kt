package com.mobile.exercise.app.di.components

import android.app.Application

import com.mobile.exercise.app.di.modules.ApplicationModule
import com.mobile.exercise.app.di.modules.ChargebackModule
import com.mobile.exercise.app.di.modules.NoticeModule
import com.mobile.exercise.app.di.modules.RetrofitModule
import com.mobile.exercise.app.ui.chargeback.ChargebackActivity
import com.mobile.exercise.app.ui.notice.NoticeActivity

import dagger.Component
import javax.inject.Singleton

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 06/10/16.
 * Copyright 2016. All rights reserved
 */
@Singleton
@Component(modules = arrayOf(
        ApplicationModule::class,
        NoticeModule::class,
        ChargebackModule::class,
        RetrofitModule::class))
interface ApplicationComponent {
    fun inject(application: Application)
    fun inject(chargebackActivity: ChargebackActivity)
    fun inject(noticeActivity: NoticeActivity)
}
