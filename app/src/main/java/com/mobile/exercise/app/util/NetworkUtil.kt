package com.mobile.exercise.app.util

import android.content.Context
import android.net.ConnectivityManager

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 16/10/16.
 * Copyright 2016. All rights reserved
 */
open class NetworkUtil {

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

}