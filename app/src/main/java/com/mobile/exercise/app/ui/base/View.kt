package com.mobile.exercise.app.ui.base

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
interface View {
    fun showProgress()
    fun hideProgress()
    fun showError(msg: String)
}