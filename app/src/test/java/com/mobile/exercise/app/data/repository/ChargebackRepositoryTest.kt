package com.mobile.exercise.app.data.repository

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.data.remote.MobileApi

import org.junit.Before
import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner
import rx.Observable
import rx.observers.TestSubscriber

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 12/10/16.
 * Copyright 2016. All rights reserved
 */
@RunWith(MockitoJUnitRunner::class)
class ChargebackRepositoryTest {

    @Mock lateinit var chargebackRepository: ChargebackRepository
    @Mock lateinit var mobileApi: MobileApi

    @Before
    fun setUp() {
        chargebackRepository = ChargebackRepositoryImpl(mobileApi)
    }

    @Test
    fun getChargeback() {
        val chargeback = Chargeback()

        Mockito.doReturn(Observable.just(chargeback))
            .`when`(mobileApi)
            .getChargebackInfo()

        val testSubscriber = TestSubscriber<Chargeback>()
        chargebackRepository.getChargeback().subscribe(testSubscriber)
        testSubscriber.assertCompleted()
        testSubscriber.assertValueCount(1)
        testSubscriber.assertValue(chargeback)
    }

    @Test
    fun getChargebackFail() {
        Mockito.doReturn(Observable.error<Any>(RuntimeException()))
                .`when`(mobileApi)
                .getChargebackInfo()

        val testSubscriber = TestSubscriber<Chargeback>()
        chargebackRepository.getChargeback().subscribe(testSubscriber)
        testSubscriber.assertError(RuntimeException::class.java)
        testSubscriber.assertNoValues()
    }

    @Test
    fun blockingCard() {
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(mobileApi)
                .blockingCard()

        val testSubscriber = TestSubscriber<ApiResponse>()
        chargebackRepository.blockingCard().subscribe(testSubscriber)
        testSubscriber.assertCompleted()
        testSubscriber.assertValue(apiResponse)
    }

    @Test
    fun blockingCardFail() {
        Mockito.doReturn(Observable.error<Any>(RuntimeException()))
                .`when`(mobileApi)
                .blockingCard()

        val testSubscriber = TestSubscriber<ApiResponse>()
        chargebackRepository.blockingCard().subscribe(testSubscriber)
        testSubscriber.assertError(RuntimeException::class.java)
        testSubscriber.assertNoValues()
    }

    @Test
    fun unblockingCard() {
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(mobileApi)
                .unblockingCard()

        val testSubscriber = TestSubscriber<ApiResponse>()
        chargebackRepository.unblockingCard().subscribe(testSubscriber)
        testSubscriber.assertCompleted()
        testSubscriber.assertValue(apiResponse)
    }

    @Test
    fun UnblockingCardFail() {
        Mockito.doReturn(Observable.error<Any>(RuntimeException()))
                .`when`(mobileApi)
                .unblockingCard()

        val testSubscriber = TestSubscriber<ApiResponse>()
        chargebackRepository.unblockingCard().subscribe(testSubscriber)
        testSubscriber.assertError(RuntimeException::class.java)
        testSubscriber.assertNoValues()
    }

    @Test
    fun submitChargeback() {
        val chargeback = Chargeback()
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(mobileApi)
                .submitChargeback(chargeback)

        val testSusbriber = TestSubscriber<ApiResponse>()
        chargebackRepository.submitChargeback(chargeback).subscribe(testSusbriber)
        testSusbriber.assertCompleted()
        testSusbriber.assertValue(apiResponse)
    }

}