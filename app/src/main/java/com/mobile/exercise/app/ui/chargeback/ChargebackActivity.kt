package com.mobile.exercise.app.ui.chargeback

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.text.Spanned
import android.view.View
import android.widget.Toast
import com.mobile.exercise.app.MobileApp

import com.mobile.exercise.app.R
import com.mobile.exercise.app.data.model.Chargeback

import kotlinx.android.synthetic.main.activity_chargeback.*
import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class ChargebackActivity: AppCompatActivity(), ChargebackView {

    @Inject lateinit var chargebackPresenter: ChargebackPresenter

    var isUnblockingCard: Boolean = false
    var reazonDetailAdapter: ReasonDetailAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chargeback)

        setupInit()
    }

    override fun onDestroy() {
        super.onDestroy()
        chargebackPresenter.detachView()
    }

    /** MVP View methods implementation **/
    override fun showProgress() {
        progress_fragment.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_fragment.visibility = View.GONE
    }

    override fun showError(msg: String) {
        showMessage(msg)
    }

    override fun populateChargeback(chargeback: Chargeback) {
        tv_title.text = chargeback.title!!.toUpperCase()

        val result: Spanned

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(chargeback.comment_hint, Html.FROM_HTML_MODE_LEGACY)
        } else {
            result = Html.fromHtml(chargeback.comment_hint)
        }

        tv_comment.hint = result
        reazonDetailAdapter!!.addList(chargeback.razonDetails)

        unblockCard(chargeback.autoblock)
    }

    override fun showChargebackInfoFailed() {
        showMessage(getString(R.string.error_loading_chargeback))
    }

    override fun showSubmitChargebackFailed() {
        showMessage(getString(R.string.error_submit_chargeback))
    }

    override fun showBlockingCardFailed() {
        showMessage(getString(R.string.error_blocking_card))
    }

    override fun showUnblockingCardFailed() {
        showMessage(getString(R.string.error_unblocking_card))
    }

    override fun cancel() {
        finish()
    }

    override fun showDialogResponse() {
        val alertDialog = AlertDialog.Builder(this)
                .setTitle(getString(R.string.dialog_confirmation_title))
                .setMessage(getString(R.string.dialog_confirmation_message))
                .setNeutralButton(R.string.dialog_action_close, null)
       alertDialog.create()
    }

    /** Helpers Methods **/
    private fun setupAdapter() {
        list_item.apply {
            setHasFixedSize(true)
            val linearLayout = LinearLayoutManager(context)
            layoutManager = linearLayout
        }

        if (list_item.adapter == null) {
            reazonDetailAdapter = ReasonDetailAdapter()
            list_item.adapter = reazonDetailAdapter
        }
    }

    private fun unblockCard(autoblock: Boolean) {
        if ( !autoblock ) {
            tv_message_card.text = getString(R.string.message_card_unblocked)
            img_llave.setImageResource(R.drawable.ic_chargeback_unlock)
            isUnblockingCard = true

            chargebackPresenter.unblockCard()
        } else {
            tv_message_card.text = getString(R.string.message_card_blocked)
            img_llave.setImageResource(R.drawable.ic_chargeback_lock)
            isUnblockingCard = false

            chargebackPresenter.blockCard()
        }
    }

    private fun submitChargeback() {
        val chargeback = Chargeback()
        chargeback.comment = tv_comment.text.toString()
        chargeback.razonDetails = reazonDetailAdapter!!.reazonList

        chargebackPresenter.submitChargeback(chargeback)
    }

    private fun setupInit() {
        MobileApp.graph.inject(this)

        chargebackPresenter.attachView(this)

        img_llave.setOnClickListener { unblockCard(isUnblockingCard) }
        btn_contestar.setOnClickListener { submitChargeback() }
        btn_cancelar.setOnClickListener { cancel() }

        setupAdapter()

        chargebackPresenter.loadChargebackInfo()
    }

    private fun showMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

}