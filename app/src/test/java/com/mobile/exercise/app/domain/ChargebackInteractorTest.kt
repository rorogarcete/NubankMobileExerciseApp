package com.mobile.exercise.app.domain

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.data.repository.ChargebackRepository

import org.junit.Before
import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner

import rx.Observable
import rx.observers.TestSubscriber

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 12/10/16.
 * Copyright 2016. All rights reserved
 */
@RunWith(MockitoJUnitRunner::class)
class ChargebackInteractorTest {

    @Mock lateinit var chargebackInteractor: ChargebackInteractor
    @Mock lateinit var chargebackRepository: ChargebackRepository

    @Before
    fun setUp() {
        chargebackInteractor = ChargebackInteractorImpl(chargebackRepository)
    }

    @Test
    fun getInfoUserChargeback() {
        val chargeback = Chargeback()

        Mockito.doReturn(Observable.just(chargeback))
                .`when`(chargebackRepository)
                .getChargeback()

        val testSubscriber = TestSubscriber<Chargeback>()
        chargebackInteractor.getInfoUserChargeback().subscribe(testSubscriber)
        testSubscriber.assertCompleted()
        testSubscriber.assertValueCount(1)
        testSubscriber.assertValue(chargeback)
    }

    @Test
    fun submitUserChargeback() {
        val chargeback = Chargeback()
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(chargebackRepository)
                .submitChargeback(chargeback)

        val testSusbriber = TestSubscriber<ApiResponse>()
        chargebackInteractor.submitUserChargeback(chargeback).subscribe(testSusbriber)
        testSusbriber.assertCompleted()
        testSusbriber.assertValue(apiResponse)
    }

    @Test
    fun blockingUserCard() {
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(chargebackRepository)
                .blockingCard()

        val testSubscriber = TestSubscriber<ApiResponse>()
        chargebackInteractor.blockingUserCard().subscribe(testSubscriber)
        testSubscriber.assertCompleted()
        testSubscriber.assertValue(apiResponse)
    }

    @Test
    fun unblockingUserCard() {
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(chargebackRepository)
                .unblockingCard()

        val testSubscriber = TestSubscriber<ApiResponse>()
        chargebackInteractor.unblockingUserCard().subscribe(testSubscriber)
        testSubscriber.assertCompleted()
        testSubscriber.assertValue(apiResponse)
    }

}