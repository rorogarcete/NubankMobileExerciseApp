package com.mobile.exercise.app.ui.chargeback

import com.mobile.exercise.app.data.model.ApiResponse

import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.domain.ChargebackInteractor
import rx.Subscriber

import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import timber.log.Timber

import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 08/10/16.
 * Copyright 2016. All rights reserved
 */
class ChargebackPresenterImpl @Inject constructor(private val chargebackInteractor: ChargebackInteractor): ChargebackPresenter {

    val TAG = ChargebackPresenterImpl::class.java.simpleName

    lateinit var chargebackView: ChargebackView
    lateinit var mSubscriptions: CompositeSubscription

    override fun attachView(t: ChargebackView) {
        this.chargebackView = t
        mSubscriptions = CompositeSubscription()
    }

    override fun detachView() {
        mSubscriptions.clear()
    }

    override fun loadChargebackInfo() {
        mSubscriptions.add(chargebackInteractor.getInfoUserChargeback()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: Subscriber<Chargeback> () {
                    override fun onCompleted() { }

                    override fun onError(e: Throwable) {
                        Timber.e("Error load chargeback Info " + e)
                        chargebackView.showChargebackInfoFailed()
                        chargebackView.hideProgress()
                    }

                    override fun onNext(chargeback: Chargeback) {
                        Timber.i("successful load chargeback Info")
                        hideLoadingAndShowChargeback(chargeback)
                    }
                }))
    }

    override fun submitChargeback(chargeback: Chargeback) {
        mSubscriptions.add(chargebackInteractor.submitUserChargeback(chargeback)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { chargebackView.showProgress() }
                .subscribe(object: Subscriber<ApiResponse> () {
                    override fun onCompleted() { }

                    override fun onError(e: Throwable) {
                        Timber.e("Error submit chargeback " + e)
                        chargebackView.showSubmitChargebackFailed()
                        chargebackView.hideProgress()
                    }

                    override fun onNext(apiResponse: ApiResponse) {
                        Timber.i("successful submit chargeback")
                        chargebackView.showDialogResponse()
                        hideLoadingAndShowResponse(apiResponse)
                    }
                }))
    }

    override fun blockCard() {
        mSubscriptions.add(chargebackInteractor.blockingUserCard()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { chargebackView.showProgress() }
                .subscribe(object: Subscriber<ApiResponse> () {
                    override fun onCompleted() { }

                    override fun onError(e: Throwable) {
                        Timber.e("Error block Card User " + e)
                        chargebackView.showBlockingCardFailed()
                        chargebackView.hideProgress()
                    }

                    override fun onNext(apiResponse: ApiResponse) {
                        Timber.i("successful block card")
                        hideLoadingAndShowResponse(apiResponse)
                    }
                }))
    }

    override fun unblockCard() {
        mSubscriptions.add(chargebackInteractor.unblockingUserCard()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { chargebackView.showProgress() }
                .subscribe(object: Subscriber<ApiResponse> () {
                    override fun onCompleted() { }

                    override fun onError(e: Throwable) {
                        Timber.e("Error Unblock Card User " + e)
                        chargebackView.showUnblockingCardFailed()
                        chargebackView.hideProgress()
                    }

                    override fun onNext(apiResponse: ApiResponse) {
                        Timber.i("successful unblock card")
                        hideLoadingAndShowResponse(apiResponse)
                    }
                }))
    }

    //Helpers Methods
    private fun hideLoadingAndShowChargeback(chargeback: Chargeback ) {
        chargebackView.showProgress()
        chargebackView.populateChargeback(chargeback)
        chargebackView.hideProgress()
    }

    private fun hideLoadingAndShowResponse(apiResponse: ApiResponse) {
        chargebackView.hideProgress()

        if (apiResponse.status != "Ok") {
            chargebackView.showError("Hubo un error inesperado. Intente mas tarde")
        }
    }

}