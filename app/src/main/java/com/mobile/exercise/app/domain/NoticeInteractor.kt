package com.mobile.exercise.app.domain

import com.mobile.exercise.app.data.model.Notice

import rx.Observable

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 04/10/16.
 * Copyright 2016. All rights reserved
 */
interface NoticeInteractor {
    fun retrieveNotice() : Observable<Notice>
}

