package com.mobile.exercise.app.domain

import com.mobile.exercise.app.data.model.Notice
import com.mobile.exercise.app.data.repository.NoticeRepository

import rx.Observable
import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 09/10/16.
 * Copyright 2016. All rights reserved
 */
class NoticeInteractorImpl @Inject constructor(private val noticeRepository: NoticeRepository): NoticeInteractor {

    override fun retrieveNotice(): Observable<Notice> {
        return noticeRepository.getNoticeInfo()
    }
}
