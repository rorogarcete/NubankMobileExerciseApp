package com.mobile.exercise.app.di.annotations

import javax.inject.Qualifier

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 13/10/16.
 * Copyright 2016. All rights reserved
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiUrl