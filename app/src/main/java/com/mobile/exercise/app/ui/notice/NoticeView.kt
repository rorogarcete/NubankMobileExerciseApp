package com.mobile.exercise.app.ui.notice

import com.mobile.exercise.app.data.model.Notice
import com.mobile.exercise.app.ui.base.View

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
interface NoticeView: View {
    fun populateNoticeInfo(notice: Notice)
    fun cancel()
    fun goToChargebackActivity()
}