package com.mobile.exercise.app.di.modules

import android.app.Application
import android.content.Context

import com.mobile.exercise.app.MobileApp

import dagger.Module
import dagger.Provides
import timber.log.Timber
import javax.inject.Singleton

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 07/10/16.
 * Copyright 2016. All rights reserved
 */
@Module
class ApplicationModule(private val app: MobileApp) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.baseContext

    @Provides
    fun provideDebugTree(): Timber.DebugTree = Timber.DebugTree()

}
