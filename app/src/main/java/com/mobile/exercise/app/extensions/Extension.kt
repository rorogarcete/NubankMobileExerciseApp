@file:JvmName("Extension")

package com.mobile.exercise.app.extensions

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mobile.exercise.app.util.SingleClickListener

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(getAdapterPosition(), getItemViewType())
    }
    return this
}

/**
 * Click listener setter that prevents double click on the view it´s set
 */
fun View.singleClick(l: (android.view.View?) -> Unit){
    setOnClickListener(SingleClickListener(l))
}
