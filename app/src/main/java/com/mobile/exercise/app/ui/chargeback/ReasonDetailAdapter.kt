package com.mobile.exercise.app.ui.chargeback

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup

import com.mobile.exercise.app.R
import com.mobile.exercise.app.data.model.ReasonDetail
import com.mobile.exercise.app.extensions.inflate
import com.mobile.exercise.app.extensions.singleClick

import kotlinx.android.synthetic.main.reason_item.view.*

import java.util.ArrayList

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class ReasonDetailAdapter: RecyclerView.Adapter<ReasonDetailAdapter.ViewHolder>() {

    var reazonList: ArrayList<ReasonDetail> = ArrayList()

    var reasonDetails: ArrayList<ReasonDetail> = ArrayList()
    var onItemClickListener: ((ReasonDetail) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.reason_item)
        return ViewHolder(view, onItemClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(reasonDetails[position])
    }

    override fun getItemCount() = reasonDetails.size

    class ViewHolder(val view: View, var onItemClickListener: ((ReasonDetail) -> Unit)?): RecyclerView.ViewHolder(view) {

        fun bind(item: ReasonDetail) = with(itemView) {
            tv_description.text = item.title

            sw_select.setOnCheckedChangeListener { compoundButton, b ->  evaluateReason(b) }
            sw_select.singleClick { onItemClickListener?.invoke(item) }
        }

        fun evaluateReason(select: Boolean) {
            if (select) {
                Log.d("Adater", "ON")

            } else {
                Log.d("Adater", "OFF")
            }
        }

    }

    fun addList(list: ArrayList<ReasonDetail>) {
        reasonDetails.addAll(list)
        reazonList.addAll(list)
    }

}