package com.mobile.exercise.app

import android.app.Application

import com.mobile.exercise.app.di.components.*
import com.mobile.exercise.app.di.modules.ChargebackModule
import com.mobile.exercise.app.di.modules.NoticeModule
import com.mobile.exercise.app.di.modules.RetrofitModule

import timber.log.Timber

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class MobileApp : Application() {

    companion object {
        lateinit var graph: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initTimber()
        initDependencyGraph()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }

    private fun initDependencyGraph() {
        graph = DaggerApplicationComponent.builder()
                .noticeModule(NoticeModule())
                .retrofitModule(RetrofitModule())
                .chargebackModule(ChargebackModule())
                .build()

        graph.inject(this)
    }

}

