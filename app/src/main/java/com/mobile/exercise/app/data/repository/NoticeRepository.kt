package com.mobile.exercise.app.data.repository

import com.mobile.exercise.app.data.model.Notice

import rx.Observable

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 10/10/16.
 * Copyright 2016. All rights reserved
 */
interface NoticeRepository {
    fun getNoticeInfo() : Observable<Notice>
}
