package com.mobile.exercise.app.data.repository

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.data.remote.MobileApi

import rx.Observable
import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 08/10/16.
 * Copyright 2016. All rights reserved
 */
class ChargebackRepositoryImpl @Inject constructor(private val mobileApi: MobileApi) : ChargebackRepository {

    override fun getChargeback(): Observable<Chargeback> {
        return mobileApi.getChargebackInfo()
    }

    override fun blockingCard(): Observable<ApiResponse> {
        return mobileApi.blockingCard()
    }

    override fun unblockingCard(): Observable<ApiResponse> {
        return mobileApi.unblockingCard()
    }

    override fun submitChargeback(chargeback: Chargeback): Observable<ApiResponse> {
        return mobileApi.submitChargeback(chargeback)
    }
}