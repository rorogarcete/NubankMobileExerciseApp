package com.mobile.exercise.app.data.model

import com.google.gson.annotations.SerializedName

import java.util.ArrayList

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class Chargeback() {
    var id: String = ""
    var title: String? = null
    var comment_hint: String? = null
    var autoblock: Boolean = false

    var comment: String? = null

    @SerializedName("reason_details")
    var razonDetails: ArrayList<ReasonDetail> = ArrayList()

}

