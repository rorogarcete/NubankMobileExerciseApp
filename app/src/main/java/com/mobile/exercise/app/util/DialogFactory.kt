package com.mobile.exercise.app.util

import android.app.Dialog
import android.content.Context
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog

import com.mobile.exercise.app.R

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 09/10/16.
 * Copyright 2016. All rights reserved
 */
open class DialogFactory {

    open fun createSimpleOkErrorDialog(context: Context, title: String, message: String): Dialog {
        val alertDialog = AlertDialog.Builder(context)
                .setTitle(title).setIconAttribute(R.color.purple)
                .setMessage(message)
                .setNeutralButton(R.string.dialog_action_close, null)
        return alertDialog.create()
    }

    open fun createSimpleOkErrorDialog(context: Context,
                                  @StringRes titleResource: Int,
                                  @StringRes messageResource: Int): Dialog {

        return createSimpleOkErrorDialog(context,
                context.getString(titleResource),
                context.getString(messageResource))
    }

    open fun createSimpleOkErrorDialog(context: Context, message: String): Dialog {
        val alertDialog = AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.dialog_error_title))
                .setMessage(message)
                .setNeutralButton(R.string.dialog_action_close, null)
        return alertDialog.create()
    }

    open fun createSimpleOkErrorDialog(context: Context,
                                  @StringRes messageResource: Int): Dialog {

        return createSimpleOkErrorDialog(context, context.getString(messageResource))
    }
}

