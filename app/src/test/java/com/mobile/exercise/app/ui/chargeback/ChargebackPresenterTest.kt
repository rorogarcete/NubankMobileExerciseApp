package com.mobile.exercise.app.ui.chargeback

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.domain.ChargebackInteractor
import com.mobile.exercise.app.util.RxSchedulersOverrideRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.runners.MockitoJUnitRunner
import rx.Observable

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
@RunWith(MockitoJUnitRunner::class)
class ChargebackPresenterTest {

    @Mock lateinit var chargebackView: ChargebackView
    @Mock lateinit var chargebackPresenter: ChargebackPresenter
    @Mock lateinit var chargebackInteractor: ChargebackInteractor

    @Rule @JvmField val mOverrideSchedulersRule = RxSchedulersOverrideRule()

    @Before
    fun setUp() {
        chargebackPresenter = ChargebackPresenterImpl(chargebackInteractor)
        chargebackPresenter.attachView(chargebackView)
    }

    @Test
    fun detachView() {
        chargebackPresenter.detachView()
    }

    @Test
    fun loadChargebackInfo() {
        val chargeback = Chargeback()
        chargeback.autoblock = true
        chargeback.comment_hint = "Nos informe"
        chargeback.title = "Detalhe da compra"

        Mockito.doReturn(Observable.just(chargeback))
            .`when`(chargebackInteractor)
            .getInfoUserChargeback()

        chargebackPresenter.loadChargebackInfo()
        Mockito.verify(chargebackView).showProgress()
        Mockito.verify(chargebackView).populateChargeback(chargeback)
        Mockito.verify(chargebackView).hideProgress()
    }

    @Test
    fun loadChargebackInfoFail() {
        val chargeback = Chargeback()
        chargeback.autoblock = true
        chargeback.comment_hint = "Nos informe"
        chargeback.title = "Detalhe da compra"

        Mockito.doReturn(Observable.error<Chargeback>(RuntimeException()))
                .`when`(chargebackInteractor)
                .getInfoUserChargeback()

        chargebackPresenter.loadChargebackInfo()
        Mockito.verify(chargebackView, Mockito.never()).populateChargeback(chargeback)
        Mockito.verify(chargebackView).showChargebackInfoFailed()
    }

    @Test
    fun submitChargeback() {
        val chargeback = Chargeback()
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(chargebackInteractor)
                .submitUserChargeback(chargeback)

        chargebackPresenter.submitChargeback(chargeback)
        Mockito.verify(chargebackView).showProgress()
        Mockito.verify(chargebackView).hideProgress()
    }

    @Test
    fun submitChargebackFail() {
        val chargeback = Chargeback()

        Mockito.doReturn(Observable.error<Chargeback>(RuntimeException()))
                .`when`(chargebackInteractor)
                .submitUserChargeback(chargeback)

        chargebackPresenter.submitChargeback(chargeback)
        Mockito.verify(chargebackView).showSubmitChargebackFailed()
    }

    @Test
    fun blockCard() {
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
            .`when`(chargebackInteractor)
            .blockingUserCard()

        chargebackPresenter.blockCard()
        Mockito.verify(chargebackView).showProgress()
        Mockito.verify(chargebackView).hideProgress()
    }

    @Test
    fun blockCardFail() {
        Mockito.doReturn(Observable.error<ApiResponse>(RuntimeException()))
                .`when`(chargebackInteractor)
                .blockingUserCard()

        chargebackPresenter.blockCard()
        Mockito.verify(chargebackView).showBlockingCardFailed()
    }

    @Test
    fun unblockCard() {
        val apiResponse = ApiResponse()

        Mockito.doReturn(Observable.just(apiResponse))
                .`when`(chargebackInteractor)
                .unblockingUserCard()

        chargebackPresenter.unblockCard()
        Mockito.verify(chargebackView).showProgress()
        Mockito.verify(chargebackView).hideProgress()
    }

    @Test
    fun unblockCardFail() {
        Mockito.doReturn(Observable.error<ApiResponse>(RuntimeException()))
                .`when`(chargebackInteractor)
                .unblockingUserCard()

        chargebackPresenter.unblockCard()
        Mockito.verify(chargebackView).showUnblockingCardFailed()
    }

}