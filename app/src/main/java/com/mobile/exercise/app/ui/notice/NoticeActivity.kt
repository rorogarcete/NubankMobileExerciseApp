package com.mobile.exercise.app.ui.notice

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.View
import android.widget.Toast

import com.mobile.exercise.app.MobileApp
import com.mobile.exercise.app.R
import com.mobile.exercise.app.data.model.Notice
import com.mobile.exercise.app.ui.chargeback.ChargebackActivity

import kotlinx.android.synthetic.main.activity_notice.*
import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class NoticeActivity : AppCompatActivity(), NoticeView {

    @Inject lateinit var noticePresentere: NoticePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notice)

        setupInit()
    }

    override fun onDestroy() {
        super.onDestroy()
        noticePresentere.detachView()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    /** MVP View methods implementation **/
    override fun showError(msg: String) {
        Toast.makeText(this, getString(R.string.error_loading_notice), Toast.LENGTH_SHORT).show()
    }

    override fun hideProgress() {
        progress.visibility = View.GONE
    }

    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }

    override fun populateNoticeInfo(notice: Notice) {
        tv_title.text = notice.title

        val result: Spanned

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(notice.description, Html.FROM_HTML_MODE_LEGACY)
        } else {
            result = Html.fromHtml(notice.description)
        }

        tv_description.text = result
    }

    override fun cancel() {
        close()
    }

    override fun goToChargebackActivity() {
        val intent = Intent(this, ChargebackActivity::class.java)
        startActivity(intent)
    }

    /** Helper method **/
    private fun setupInit() {
        MobileApp.graph.inject(this)

        noticePresentere.attachView(this)

        btn_continue.setOnClickListener { goToChargebackActivity() }
        btn_close.setOnClickListener { close() }

        noticePresentere.loadNoticeInfo()
    }

    private fun close() {
        finish()
    }
}
