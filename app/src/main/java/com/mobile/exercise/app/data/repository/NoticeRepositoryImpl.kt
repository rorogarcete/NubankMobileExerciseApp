package com.mobile.exercise.app.data.repository

import com.mobile.exercise.app.data.model.Notice
import com.mobile.exercise.app.data.remote.MobileApi

import rx.Observable
import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 11/10/16.
 * Copyright 2016. All rights reserved
 */
class NoticeRepositoryImpl @Inject constructor(private val mobileApi: MobileApi): NoticeRepository {

    override fun getNoticeInfo(): Observable<Notice> {
        return mobileApi.getNoticeInfo()
    }

}