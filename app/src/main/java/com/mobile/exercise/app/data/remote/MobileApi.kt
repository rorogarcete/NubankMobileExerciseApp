package com.mobile.exercise.app.data.remote

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.data.model.Notice

import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

import rx.Observable

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 04/10/16.
 * Copyright 2016. All rights reserved
 */
interface MobileApi {

    @GET("/notice")
    fun getNoticeInfo(): Observable<Notice>

    @GET("/chargeback")
    fun getChargebackInfo(): Observable<Chargeback>

    @POST("/chargeback")
    fun submitChargeback(@Body chargeback: Chargeback): Observable<ApiResponse>

    @POST("/card_block")
    fun blockingCard(): Observable<ApiResponse>

    @POST("/card_unblock")
    fun unblockingCard(): Observable<ApiResponse>

}