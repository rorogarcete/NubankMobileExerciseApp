package com.mobile.exercise.app.ui.chargeback

import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.ui.base.Presenter

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
interface ChargebackPresenter: Presenter<ChargebackView> {
    fun loadChargebackInfo()
    fun blockCard()
    fun unblockCard()
    fun submitChargeback(chargeback: Chargeback)
}
