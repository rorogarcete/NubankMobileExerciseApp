package com.mobile.exercise.app.domain

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.data.repository.ChargebackRepository

import rx.Observable
import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class ChargebackInteractorImpl @Inject constructor(private val chargebackRepository: ChargebackRepository): ChargebackInteractor {

    override fun getInfoUserChargeback(): Observable<Chargeback> {
        return chargebackRepository.getChargeback()
    }

    override fun submitUserChargeback(chargeback: Chargeback): Observable<ApiResponse> {
        return chargebackRepository.submitChargeback(chargeback)
    }

    override fun blockingUserCard(): Observable<ApiResponse> {
        return chargebackRepository.blockingCard()
    }

    override fun unblockingUserCard(): Observable<ApiResponse> {
        return chargebackRepository.unblockingCard()
    }
}
