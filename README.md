# Mobile Hiring Exercise

## Requirements
- JDK 1.8
- Android SDK (API 24)
- Latest Android SDK Tools and build tools.
- Plugins for Android Studio are also required: Kotlin for Android

## Libraries and tools included:
- Support libraries
- RecyclerViews
- [RxJava](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/RxAndroid)
- [Retrofit 2](http://square.github.io/retrofit/) 
- [Dagger 2](http://square.github.io/retrofit/)
- [Timber](https://github.com/JakeWharton/timber)
- Unit test with [JUnit]( http://junit.org/junit4/) and [Mockito](http://mockito.org/)
- Functional tests with [Espresso](https://google.github.io/android-testing-support-library/docs/espresso/index.html)

## Tests
- To run unit tests on your machine: 
./gradlew test

- To run functional tests on connected devices:
./gradlew connectedAndroidTest