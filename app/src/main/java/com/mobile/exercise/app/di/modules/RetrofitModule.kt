package com.mobile.exercise.app.di.modules

import com.mobile.exercise.app.BuildConfig
import com.mobile.exercise.app.data.remote.MobileApi
import com.mobile.exercise.app.di.annotations.ApiUrl

import dagger.Module
import dagger.Provides

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 14/10/16.
 * Copyright 2016. All rights reserved
 */
@Module
class RetrofitModule {

    @ApiUrl
    @Provides
    @Singleton
    fun provideApiUrl(): String {
        return "https://nu-mobile-hiring.herokuapp.com"
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                            else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
    }

    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(@ApiUrl url: String, okHttpClient: OkHttpClient, converterFactory: Converter.Factory): Retrofit {
        return Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideMobileApi(retrofit: Retrofit): MobileApi {
        return retrofit.create(MobileApi::class.java)
    }

}