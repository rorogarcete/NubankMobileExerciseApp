package com.mobile.exercise.app.data.model

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class Notice {
    var title:String = ""
    var description:String = ""
}
