package com.mobile.exercise.app.ui.chargeback

import com.mobile.exercise.app.data.model.Chargeback
import com.mobile.exercise.app.ui.base.View

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
interface ChargebackView: View {
    fun cancel()
    fun showDialogResponse()
    fun populateChargeback(chargeback: Chargeback)
    fun showChargebackInfoFailed()
    fun showBlockingCardFailed()
    fun showUnblockingCardFailed()
    fun showSubmitChargebackFailed()
}
