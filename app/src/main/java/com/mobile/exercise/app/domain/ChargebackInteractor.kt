package com.mobile.exercise.app.domain

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback

import rx.Observable

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 04/10/16.
 * Copyright 2016. All rights reserved
 */
interface ChargebackInteractor {
    fun getInfoUserChargeback(): Observable<Chargeback>
    fun blockingUserCard(): Observable<ApiResponse>
    fun unblockingUserCard(): Observable<ApiResponse>
    fun submitUserChargeback(chargeback: Chargeback): Observable<ApiResponse>
}
