package com.mobile.exercise.app.data.model

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by rorogarcete on 15/10/16.
 */
class ChargebackTest {

    @Test
    fun testInitWithEmptyTitle() {
        //when
        val chargeback = Chargeback()
        chargeback.title = null

        //then
        assertNull(chargeback.title)
    }

    @Test
    fun testInitWithAutoblockIsFalse() {
        //when
        val chargeback = Chargeback()
        chargeback.autoblock = false

        //then
        assertFalse(chargeback.autoblock)
    }

    @Test
    fun testInitWithAutoblockNotIsFalse() {
        //when
        val chargeback = Chargeback()
        chargeback.autoblock = true

        //then
        assertEquals(chargeback.autoblock, true)
    }

    @Test
    fun testInitWithEmptyCommentHint() {
        //when
        val chargeback = Chargeback()
        chargeback.comment_hint = null

        //then
        assertNull(chargeback.comment_hint)
    }

}