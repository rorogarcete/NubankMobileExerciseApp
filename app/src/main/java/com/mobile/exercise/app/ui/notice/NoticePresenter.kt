package com.mobile.exercise.app.ui.notice

import com.mobile.exercise.app.ui.base.Presenter

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 05/10/16.
 * Copyright 2016. All rights reserved
 */
interface NoticePresenter : Presenter<NoticeView> {
    fun loadNoticeInfo()
}