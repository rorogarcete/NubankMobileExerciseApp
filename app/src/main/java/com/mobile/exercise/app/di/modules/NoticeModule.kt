package com.mobile.exercise.app.di.modules

import com.mobile.exercise.app.data.remote.MobileApi
import com.mobile.exercise.app.data.repository.NoticeRepository
import com.mobile.exercise.app.data.repository.NoticeRepositoryImpl
import com.mobile.exercise.app.domain.NoticeInteractor
import com.mobile.exercise.app.domain.NoticeInteractorImpl
import com.mobile.exercise.app.ui.notice.NoticePresenter
import com.mobile.exercise.app.ui.notice.NoticePresenterImpl

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 12/10/16.
 * Copyright 2016. All rights reserved
 */
@Module
class NoticeModule {

    @Provides
    @Singleton
    fun provideNoticePresenter(noticeInteractor: NoticeInteractor): NoticePresenter {
        return NoticePresenterImpl(noticeInteractor)
    }

    @Provides
    @Singleton
    fun provideNoticeInteractor(noticeRepository: NoticeRepository): NoticeInteractor {
        return NoticeInteractorImpl(noticeRepository)
    }

    @Provides
    @Singleton
    fun provideNoticeRepository(mobileApi: MobileApi): NoticeRepository {
        return NoticeRepositoryImpl(mobileApi)
    }
}