package com.mobile.exercise.app.data.repository

import com.mobile.exercise.app.data.model.ApiResponse
import com.mobile.exercise.app.data.model.Chargeback

import rx.Observable

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 05/10/16.
 * Copyright 2016. All rights reserved
 */
interface ChargebackRepository {
    fun getChargeback(): Observable<Chargeback>
    fun blockingCard(): Observable<ApiResponse>
    fun unblockingCard(): Observable<ApiResponse>
    fun submitChargeback(chargeback: Chargeback): Observable<ApiResponse>
}