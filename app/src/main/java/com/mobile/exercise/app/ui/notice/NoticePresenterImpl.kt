package com.mobile.exercise.app.ui.notice

import android.util.Log

import com.mobile.exercise.app.data.model.Notice
import com.mobile.exercise.app.domain.NoticeInteractor

import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 03/10/16.
 * Copyright 2016. All rights reserved
 */
class NoticePresenterImpl @Inject constructor(private val noticeInteractor: NoticeInteractor): NoticePresenter {

    val TAG = NoticePresenterImpl::class.java.simpleName

    lateinit var noticeView: NoticeView
    lateinit var subscription: Subscription

    override fun attachView(t: NoticeView) {
        this.noticeView = t
    }

    override fun detachView() {
        subscription.unsubscribe()
    }

    override fun loadNoticeInfo() {
        subscription = noticeInteractor.retrieveNotice()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { notice -> hideLoadingAndShowNotice(notice) },
                            { t -> handleError(t) })
    }

    //Helpers Methods
    private fun hideLoadingAndShowNotice(notice: Notice) {
        noticeView.showProgress()
        noticeView.populateNoticeInfo(notice)
        noticeView.hideProgress()
    }

    private fun handleError(t: Throwable) {
        Log.e(TAG, t.message)
        noticeView.hideProgress()
        noticeView.showError(t.message)
    }

}
