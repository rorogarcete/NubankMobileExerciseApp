package com.mobile.exercise.app.di.modules

import com.mobile.exercise.app.data.remote.MobileApi
import com.mobile.exercise.app.data.repository.ChargebackRepository
import com.mobile.exercise.app.data.repository.ChargebackRepositoryImpl
import com.mobile.exercise.app.domain.ChargebackInteractor
import com.mobile.exercise.app.domain.ChargebackInteractorImpl
import com.mobile.exercise.app.ui.chargeback.ChargebackPresenter
import com.mobile.exercise.app.ui.chargeback.ChargebackPresenterImpl

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 13/10/16.
 * Copyright 2016. All rights reserved
 */
@Module
class ChargebackModule {

    @Provides
    @Singleton
    fun provideChargebackPresenter(chargebackInteractor: ChargebackInteractor): ChargebackPresenter {
        return ChargebackPresenterImpl(chargebackInteractor)
    }

    @Provides
    @Singleton
    fun provideChargebackInteractor(chargebackRepository: ChargebackRepository): ChargebackInteractor {
        return ChargebackInteractorImpl(chargebackRepository)
    }

    @Provides
    @Singleton
    fun provideChargebackRepository(mobileApi: MobileApi): ChargebackRepository {
        return ChargebackRepositoryImpl(mobileApi)
    }
}