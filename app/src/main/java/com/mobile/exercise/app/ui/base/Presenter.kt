package com.mobile.exercise.app.ui.base

/**
 * @author Rodrigo Garcete
 * @version 0.0.1
 * @since 04/10/16.
 * Copyright 2016. All rights reserved
 */
interface Presenter<T : View> {
    fun attachView(t: T)
    fun detachView()
}